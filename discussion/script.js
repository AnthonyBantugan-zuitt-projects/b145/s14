// This displays the message
console.log('Hello')

let clientName = 'Juan dela Cruz'
let contactNumber = '09232312242'

// Peeking inside a variable

let greetings;
console.log(clientName);
console.log(contactNumber);
console.log(greetings);
let pangalan = 'John Doe'
console.log(pangalan);

// DATA TYPES  
// 3 FOrms of Data Types:
/* Primitive -> contains only a single value:
                ex. strings, numbers, boolean
   Composite -> can contain multiple values
                ex. arrays, objects
   Special: 
                ex. null, undefined
*/

// 1. Strings
let country = "Philippines";
let province = 'NCR';

// 5. Arrays use []
//     Arrays are a special kind of composite data type that is used to store multiple values.


// lets create a collection of all you subjects in the bootcamp

let bootcampSubject = ["HTML", "CSS", "Bootstrap", "JavaScript"];

// display the output of the array inside the console
console.log (bootcampSubject);

// Rule of TUmb when declaring array structures
// ==> Storing multiple data types inside an array is NOT recommended. In a context of programming this does not make any sense.

// an array should be a collection of data that describes a similar/single topic or subject.
let details = ["Keanu", "Reeves", 32, true];
console.log (details)

// OBJECTS use {}
    // Objects are another special kind of composite data type that is used to mimin or represent a real world object/item.
    // They are used to create complex data that contains pieces of information that are relevant to each other.
    // Every individual piece of code or information is called property of an object.
// SYNTAX:
    // let/const onjectName = {
    //     key -> value
    //     propertyA: value,
    //     propertyB: value
    // }
// lets create an object that describes the properties of a cellphone.
    let cellphone = {
        brand: 'Samsung',
        model: 'A12',
        color: 'Black',
        serialNo: 'AX12002122',
        isHomeCredit: true,
        features: ["Calling", "Texting", "Ringing", "5G"],
        price: 8000
    }

// lets display the object inside the console
console.log (cellphone);

// Variable and Constants

// Variables -> are used to store data.

// the values/info stored inside a variablecan be changed or repackaged.
let personName = "Michael";
console.log(personName);

// if youre going to reassign a new value for a variable, you no longer have to use the "let" again.

personName = "Jordan";
console.log(personName);


// concatenate strings
    // => join, combine, link
let pet = "dog";
console.log("this is the initial value: " + pet);

pet = "cat";
console.log("This is the new value or the var: " + pet);

// Constants
    // => Permanent, fixed, absolute
// => the value assigned on a constant cannot be changed.
// syntax: const desiredName = value;
const PI = 3.14;
console.log(PI);

const year = 12;
console.log(year);

const familyName = 'Dela Cruz';
console.log(familyName);

// lets try to reassign a new value in a constant. *Will get error
// familyName = 'Castillo';
// console.log(familyName);
